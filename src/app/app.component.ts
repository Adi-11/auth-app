import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './_models';
import { AuthenticationService } from './_services';
import { AuthGuard } from './_helper/auth.guard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'front-end-auth';
  isAuthenticated: boolean;
  // currentUser: User = new User();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    // this.isAuth(authenticationService);
  }

  // async isAuth(e) {
  //   await e.currentUser.subscribe((x) => (this.currentUser = x));
  // }

  logout() {
    this.authenticationService.logout();
    this.isAuthenticated = true;
    this.router.navigate(['/login']);
  }
}
